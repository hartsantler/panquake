# PanjeaQuake

Panjea.com: [https://www.panjea.com/][homepage]
Based on: [https://ezquake.github.io/][ezquake]


## Features

 * Modern graphics
 * [QuakeTV][qtv] support
 * Rich menus
 * Multiview support
 * Tons of features to serve latest pro-gaming needs
 * Built in server browser & MP3 player control
 * Recorded games browser
 * Customization of all possible graphics elements of the game including Heads Up Display
 * All sorts of scripting possibilities
 * Windows, Linux, MacOSX and FreeBSD platforms supported (SDL2).


## Installation guide

To play Quakeworld you need the files *pak0.pak* and *pak1.pak* from the original Quake-game.

### Install PanQuake to an existing Quake-installation
If you have an existing Quake-installation simply copy the panquake executable into your Quake-directory.


### Minimal clean installation
If you want to make a clean installation of ezQuake you can do this by following these steps:

1. Create a new directory
2. Copy the panquake executable into this directory
3. Create a subdirectory called *id1*
4. Copy *pak0.pak* and *pak1.pak* into this subdirectory

## Compiling

### Compiling a Windows binary

#### Using Ubuntu Bash

You can use the new Ubuntu Bash feature in Windows 10 to compile PanjeaQuake for Windows.

To enable Bash for Windows, press the `Start` button and type `Turn Windows f` and select `Turn Windows features on or off`. Scroll down to `Windows Subsystem for Linux (Beta)` and enable it.

Now press WINDOWS+I, go to `Update & security` and then to the `For developers` tab. Enable `Developer mode`.

Now press the `Start` button again and enter `bash`. Click it and install Bash.

Enter the following command to install all required prerequisites to build ezQuake:

```
sudo apt-get install -y git mingw-w64 build-essential
```

Now clone the PanQuake source code:

```
git clone https://gitlab.com/hartsantler/panquake.git
```

Now build the PanQuake executable:

```
EZ_CONFIG_FILE=.config_windows make
```

Copy the compiled binary to your Quake folder, the binary is called `panquake.exe`.

#### Using a Linux system

Make sure you have mingw32 toolchain installed. On Arch Linux it's `mingw-w64` (select complete group).

Build an executable using the following command:

```
EZ_CONFIG_FILE=.config_windows make
```

You can add `-jN` as a parameter to `make` to build in parallell. Use number of cpu cores plus 1 (e.g. `-j5` if you have a quad core processor).

### Compiling a Linux binary

_These instructions were tested on Ubuntu_

Make sure you have the dependencies installed:

- For *Ubuntu 15.10-16.04*
```
sudo apt-get install git build-essential libsdl2-2.0-0 libsdl2-dev libjansson-dev libexpat1-dev libcurl4-openssl-dev libpng12-dev libjpeg-dev libspeex-dev libspeexdsp-dev
```
- For *Ubuntu 16.10+*
```
sudo apt install git build-essential libsdl2-2.0-0 libsdl2-dev libjansson-dev libexpat1-dev libcurl4-openssl-dev libpng-dev libjpeg-dev libspeex-dev libspeexdsp-dev
```

Clone the git repository:
```
git clone https://gitlab.com/hartsantler/panquake.git
```

Switch to `panquake` path:
```
 cd ~/panquake/
```
Run the compilation:
```
make
```

Copy the compiled binary to your Quake folder, on 64bit linux the binary will be called `panquake-linux-x86_64`.

### Compiling an OS X binary

_These instructions were tested on Mac OS X 10.10._

Get [Homebrew](http://brew.sh)

Run exactly as it says on the front page:

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Make sure you run the `brew doctor` as instructed before doing anything else.

Then run:

```
brew install sdl2 sdl2_net sdl2_image sdl2_gfx sdl2_mixer pcre jansson pkg-config speex speexdsp
```

When it's done, just run `make` and it should compile without errors.


#### Creating an app bundle

Call from main panquake directory, e.g. you probably do something like this:

```
make
sh misc/install/create_osx_bundle.sh
```

Current directory should have an `ezQuake.app` folder which is the app.

There will also be an `ezquake.zip` which basically just zips up the .app.

